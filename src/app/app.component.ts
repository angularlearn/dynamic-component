import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    template: `<h1> Hello {{name}}</h1><div><container-component></container-component></div>`,
})
export class AppComponent {
    name = 'Angular';
}
