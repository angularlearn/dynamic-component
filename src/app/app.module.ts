import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ContainerComponent } from './container/container.component';
import { InnerComponent } from './inner/inner.component';
import { Inner2Component } from './inner2/inner2.component';

@NgModule({
  declarations: [
    AppComponent,
    ContainerComponent,
    InnerComponent,
    Inner2Component
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    InnerComponent,
    Inner2Component
  ]
})
export class AppModule { }
