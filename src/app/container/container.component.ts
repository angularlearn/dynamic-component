import {
  Component,
  ComponentRef,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  Type,
  OnInit,
  OnDestroy
} from '@angular/core';

import { InnerComponent } from "../inner/inner.component";
import { Inner2Component } from "../inner2/inner2.component";

@Component({
    selector: 'container-component',
    template: `<div class="container">
    <div #componentsContainer></div>
</div>`
})
export class ContainerComponent implements OnInit {
  @ViewChild('componentsContainer', {read: ViewContainerRef}) componentsContainer: ViewContainerRef;

  private innerRef: ComponentRef<any>;
  private innerRef2: ComponentRef<any>;


  constructor(private resolver: ComponentFactoryResolver){
  }

  ngOnInit(): void {
    const innerComponentFactory = this.resolver.resolveComponentFactory(InnerComponent);
    this.innerRef = this.componentsContainer.createComponent(innerComponentFactory);

    const innerComponentFactory2 = this.resolver.resolveComponentFactory(Inner2Component);
    this.innerRef2 = this.componentsContainer.createComponent(innerComponentFactory2);
  }

  ngOnDestroy(): void {
    if(this.innerRef){
      this.innerRef.destroy();
    }

    if(this.innerRef){
      this.innerRef2.destroy();
    }
  }
}
